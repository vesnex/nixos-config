# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    # Include ZSH configuration.
    ./zsh.nix
    # User management.
    ./users.nix
  ];


  # Allows for unfree software (software not covered by the GPL and similar licenses).
  nixpkgs.config.allowUnfree = true;

  # Enables the Nix User Repository (NUR).
  /* nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
      inherit pkgs;
    };
  }; */


  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # System build dependencies.
    binutils gcc gnumake openssl pkgconfig

    # Basic system stuff.
    wget vim tree mkpasswd tmux alacritty keepassxc
    vlc transmission transmission-gtk okular ark exa
    shutter screenfetch nixops

    # Some weird dependencies for creating nix pkgs.
    carnix
    nix-prefetch-git
    appimage-run

    # Alternative to git.
    pijul

    # Web.
    megasync firefox-devedition-bin-unwrapped

    # Code and text editors.
    atom kate vscode emacs ghostwriter

    # Chat.
    weechat discord keybase-gui tdesktop
    # riot-desktop not working as of 4/1/2020
    # riot-desktop
    zoom-us

    # Blogging
    zola

    # Programming languages and dependencies.
    rustup python3 python27Full
    # Stuff needed to build C and C++ sources.
    cmake gnumake gcc
    # The rascally haskell dependency.
    (haskellPackages.ghcWithHoogle (self: [
      self.ghc
      self.random
    ]))
  ];

  environment.pathsToLink = [ "/share/zsh" ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  networking.networkmanager.enable = true;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp2s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = [ pkgs.epson-escpr pkgs.epson-escpr2 ];
  };

  # Enable avahi to find wireless printers.
  services.avahi = {
    enable = true;
    nssmdns = true;
  };

  services.keybase.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us";
  services.xserver.xkbOptions = "eurosign:e";
  services.xserver.videoDrivers = [ "displaylink" ];

  services.xserver.displayManager.lightdm.enable = true;

  # Enable the KDE Desktop Environment.
  services.xserver.desktopManager.plasma5.enable = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

  /* virtualisation.docker.enable = true; */
  /* virtualisation.libvirtd.enable = true; */
  virtualisation.virtualbox.host.enable = true;

}
