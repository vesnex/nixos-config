{ pkgs, ... }:

{
  imports = [
    # Home manager.
    <home-manager/nixos>
  ];

  users.motd = "Remember to breath.";
  users.mutableUsers = true;

  users.defaultUserShell = pkgs.zsh;

  users.users = {
    # System administrator.
    root = {
      shell = pkgs.bash;
    };
    # Logan's account.
    logan = {
      isNormalUser = true;
      createHome = true;
      home = "/home/logan";
      description = "Logan";
      uid = 1000;
      group = "users";
      shell = pkgs.zsh;
      extraGroups = [ "wheel" "networkmanager" ]; # Enable ‘sudo’ for the user.
    };
    # Used for a notebook configuration.
    notebook = {
      isNormalUser = true;
      createHome = true;
      home = "/home/notebook";
      description = "Notebook environment";
      uid = 6969;
      group = "users";
      shell = pkgs.zsh;
      extraGroups = [ "wheel" "networkmanager" ];
    };
  };

  home-manager.users.logan = { pkgs, ... }: {
    home.packages = [
      pkgs.htop
      pkgs.fortune
      pkgs.gnupg
      # Mastodon CLI tool.
      pkgs.toot
    ];
    programs.git = {
      enable = true;
      userName = "Logan Saether";
      userEmail = "x@logansaether.com";
      signing.key = "3C9BF084B26C658D";
    };
  };

  home-manager.users.notebook = { pkgs, ... }: {
    home.packages = [
    pkgs.htop
    pkgs.fortune
    pkgs.gnupg
    # Mastodon CLI tool.
    pkgs.toot
    ];
    programs.git = {
      enable = true;
      userName = "Logan Saether";
      userEmail = "x@logansaether.com";
    };
  };

}
