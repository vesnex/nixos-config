{ config, lib, pkgs, ... }:

{
  # Enable zsh.
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    histSize = 10000;
    ohMyZsh = {
      enable = true;
      plugins = [ "git" "python" "man" ];
      theme = "af-magic";
    };
    syntaxHighlighting.enable = true;
  };
}
