# NixOS Configuration

## Installing

To install the NixOS configuration clone this repository to a fresh install of NixOS
to the `/etc/nixos` directory using a Root user.

```
$ cd /etc/nixos
$ git clone https://gitlab.com/vesnex/nixos-config.git
$ nixos-rebuild switch
```

